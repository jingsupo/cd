import subprocess
from tqdm import tqdm


# 利用lzop解压文件
out = subprocess.run('lzop -d test.lzo', shell = True)

if out.returncode == 0:# 解压完成
    with open('test', 'r') as f, open('test.csv', 'w+') as f_out:# 读取解压后的文件，处理后写入新文件
        for i in tqdm(f):
            f_out.write(i)
