from __future__ import division
from __future__ import print_function

import os
import sys

from pyod.models.kde import KDE
from pyod.models.ocsvm import OCSVM
from pyod.models.iforest import IForest
from pyod.models.auto_encoder_torch import AutoEncoder
from pyod.models.cof import COF
from pyod.models.copod import COPOD
from pyod.models.cblof import CBLOF
from pyod.models.ecod import ECOD
from pyod.models.gmm import GMM
from pyod.models.hbos import HBOS
from pyod.models.inne import INNE
from pyod.models.knn import KNN
from pyod.models.kpca import KPCA
from pyod.models.lmdd import LMDD
from pyod.models.loci import LOCI
from pyod.models.loda import LODA
from pyod.models.lof import LOF
from pyod.models.lscp import LSCP
from pyod.models.lunar import LUNAR
from pyod.models.mcd import MCD
from pyod.models.rod import ROD
from pyod.models.sod import SOD
from pyod.models.sos import SOS

from pyod.models.thresholds import ALL
from pyod.utils.data import generate_data, generate_data_clusters
from pyod.utils.data import evaluate_print
from pyod.utils.example import visualize


def od(clf_name, clf):
    contamination = 0.1  # percentage of outliers
    n_train = 200  # number of training points
    n_test = 100  # number of testing points
    n_features = 2

    if clf_name == 'AutoEncoder':
        contamination = 0.1  # percentage of outliers
        n_train = 20000  # number of training points
        n_test = 2000  # number of testing points
        n_features = 300  # number of features

    # Generate sample data
    X_train, X_test, y_train, y_test = \
        generate_data(n_train=n_train,
                      n_test=n_test,
                      n_features=n_features,
                      contamination=contamination,
                      random_state=42)#behaviour="new" kpca
    if clf_name == 'GMM':
        # Generate sample data
        X_train, X_test, y_train, y_test = generate_data_clusters(
            n_train=n_train,
            n_test=n_test,
            n_features=2,
            n_clusters=4,
            contamination=contamination,
            random_state=42,
        )

    # train detector
    if clf_name == 'KDE':
        clf = KDE(contamination=ALL())
    elif clf_name == 'AutoEncoder':
        clf = AutoEncoder(epochs=10)
    elif clf_name == 'COF':
        clf = COF(n_neighbors=30)
    elif clf_name == 'CBLOF':
        clf = CBLOF(random_state=42)
    elif clf_name == 'GMM':
        clf = GMM(n_components=4)
    elif clf_name == 'INNE':
        clf = INNE(contamination=contamination, max_samples=4)
    elif clf_name == 'KNN (mahalanobis distance)':
        # calculate covariance for mahalanobis distance
        X_train_cov = np.cov(X_train, rowvar=False)
        clf = KNN(algorithm='auto', metric='mahalanobis',
                  metric_params={'V': X_train_cov})
    elif clf_name == 'LMDD':
        clf = LMDD(random_state=42)
    elif clf_name == 'LSCP':
        detector_list = [LOF(n_neighbors=15), LOF(n_neighbors=20),
                         LOF(n_neighbors=25), LOF(n_neighbors=35)]
        clf = LSCP(detector_list, random_state=42)
    else:
        clf = clf()
    clf.fit(X_train)

    # get the prediction labels and outlier scores of the training data
    y_train_pred = clf.labels_  # binary labels (0: inliers, 1: outliers)
    y_train_scores = clf.decision_scores_  # raw outlier scores

    # get the prediction on the test data
    y_test_pred = clf.predict(X_test)  # outlier labels (0 or 1)
    y_test_scores = clf.decision_function(X_test)  # outlier scores

    # evaluate and print the results
    print("\nOn Training Data:")
    evaluate_print(clf_name, y_train, y_train_scores)
    print("\nOn Test Data:")
    evaluate_print(clf_name, y_test, y_test_scores)

    if hasattr(clf, 'feature_importances_'):
        # example of the feature importance
        feature_importance = clf.feature_importances_
        print("Feature importance", feature_importance)

    # visualize the results
    visualize(clf_name, X_train, y_train, X_test, y_test, y_train_pred,
              y_test_pred, show_figure=True, save_figure=False)


clf_name = [
    'KDE',
    'OCSVM',#
    'IForest',
    'AutoEncoder',
    """Example of using Connectivity-Based Outlier Factor (COF)
     for outlier detection
    """
    'COF',#
    """Example of using Copula Based Outlier Detector (COPOD) for outlier detection
    """
    'COPOD',
    """Example of using Cluster-based Local Outlier Factor (CBLOF) for outlier
    detection
    """
    'CBLOF',
    """Example of using ECOD for outlier detection
    """
    'ECOD',
    """Example of using GMM for outlier detection
    """
    'GMM',
    """Example of using Histogram- based outlier detection (HBOS) for
    outlier detection
    """
    'HBOS',
    """Example of using INNE for outlier detection
    """
    'INNE',#
    """Example of using kNN for outlier detection
    """
    'KNN',#
    """Example of using kNN with mahalanobis distance for outlier detection
    """
    'KNN (mahalanobis distance)',#
    """Example of outlier detection based on Kernel PCA.
    """
    'KPCA',#
    """Example of using Linear Method Deviation-base outlier detection (LMDD)
    """
    'LMDD',
    """Example of using Local Correlation Integral (LOCI) for outlier detection
    """
    'LOCI',
    """Example of using LODA for outlier detection
    """
    'LODA',#
    """Example of using LOF for outlier detection
    """
    'LOF',#
    """Example of using LSCP for outlier detection
    """
    'LSCP',
    """Example of using LUNAR for outlier detection
    """
    'LUNAR',
    """Example of using Minimum Covariance Determinant (MCD) for outlier detection
    """
    'MCD',#
    """Example of using ROD for outlier detection
    """
    'ROD',
    """Example of using SOD for outlier detection
    """
    # Note that SOD is meant to work in high dimensions d > 2.
    # But here we are using 2D for visualization purpose
    # thus, higher precision is expected in higher dimensions
    'SOD',
    """Example of using Stochastic Outlier Selection (SOS) for outlier detection
    """
    'SOS',
]

for cn in clf_name:
    if cn == 'KNN (mahalanobis distance)':
        clf = None
    else:
        clf = eval(cn)
    od(cn, clf)
