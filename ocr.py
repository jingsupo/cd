import datetime
import os

import cv2
import numpy as np
import paddlehub as hub
from PIL import Image
from paddleocr import PaddleOCR, draw_ocr
from tqdm import tqdm


class IstOcr:
    # PaddleOCR目前支持中英文、英文、法语、德语、韩语、日语，可以通过修改lang参数进行切换
    # 参数依次为`ch`, `en`, `french`, `german`, `korean`, `japan`。
    ocr_whl = PaddleOCR(use_angle_cls=True, lang="ch")  # need to run only once to download and load model into memory
    # 加载预训练模型
    ocr_mobile = hub.Module(name="chinese_ocr_db_crnn_mobile")
    # ocr_server = hub.Module(name="chinese_ocr_db_crnn_server")
    ocr_hub = ocr_mobile

    @classmethod
    def ocr_result(cls, img_path, print_line=False):
        result = cls.ocr_whl.ocr(img_path, cls=True)
        if print_line:
            for line in result:
                print(line)
        return result

    @classmethod
    def ocr_show(cls, img_path, result):
        # 显示结果
        image = Image.open(img_path).convert('RGB')
        boxes = [line[0] for line in result]
        texts = [line[1][0] for line in result]
        scores = [line[1][1] for line in result]
        im_show = draw_ocr(image, boxes, texts, scores, font_path=r'D:\lib\PaddleOCR\doc\fonts\simfang.ttf')
        im_show = Image.fromarray(im_show)
        output_dir = 'results'
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        file_path = f"""{output_dir}/{os.path.basename(img_path).split('.')[0] + '-' +
                                      datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}.jpg"""
        im_show.save(file_path)
        return im_show

    @classmethod
    def ocr_result_hub(cls, img_path):
        # 读取图片
        if isinstance(img_path, np.ndarray):
            np_images = [img_path]
        else:
            np_images = [cv2.imread(img_path)]
        # 识别文本
        results = cls.ocr_hub.recognize_text(
            images=np_images,  # 图片数据，ndarray.shape为[H, W, C]，BGR格式
            use_gpu=False,  # 是否使用GPU；若使用GPU，请先设置CUDA_VISIBLE_DEVICES环境变量
            output_dir='ocr_result',  # 图片的保存路径，默认设为ocr_result
            visualization=True,  # 是否将识别结果保存为图片文件
            box_thresh=0.5,  # 检测文本框置信度的阈值
            text_thresh=0.5)  # 识别中文文本置信度的阈值
        return results


def get_ocr_result(img_path, mode='whl'):
    if mode == 'whl':
        return IstOcr.ocr_result(img_path)
    elif mode == 'hub':
        return IstOcr.ocr_result_hub(img_path)
    else:
        raise Exception("param mode must be in ['whl', 'hub']")


input_path = 'wxsl_imgs'
output_path = 'wxsl_texts'

for img in os.listdir(input_path):
    print(img)
    result = get_ocr_result(os.path.join(input_path, img))
    for r in result:
        text = []
        for i in r:
            text.append(i[1][0])
        open(os.path.join(output_path, f'{img.split(".")[0]}.txt'), 'a').write('\n'.join(text))

