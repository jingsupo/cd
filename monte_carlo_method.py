import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False


x = np.linspace(0, 10, 100)
y = np.polyval([0.5, -2, 1], x)
y += 0.5*np.random.randn(len(x))

n_iterations = 1000 # number of iterations
params = np.zeros((n_iterations, 3)) # matrix to store parameter values
mse_values = np.zeros((n_iterations, 1)) # vector to store MSE values

for i in range(n_iterations):
    # generate random parameters
    a = np.random.randn()
    b = np.random.randn()
    c = np.random.randn()
    params[i, :] = [a, b, c]

    # calculate y values for the random parameters
    y_fit = np.polyval([a, b, c], x)

    # calculate MSE between y values and true data
    mse_values[i] = np.mean((y - y_fit)**2)

# find parameters with lowest MSE
best_idx = np.argmin(mse_values)  # best_mse
best_params = params[best_idx, :]
best_fit = np.polyval(best_params, x)

plt.plot(x, y, 'o')
plt.plot(x, best_fit, '-')
plt.legend(['数据', '最佳拟合'])
plt.xlabel('x')
plt.ylabel('y')
