package main

import (
	"fmt"

	"gitee.com/jingsupo/goutils/utils"
	"github.com/xuri/excelize/v2"
)

func main() {
	path := "D:\\Desktop\\result\\99_2.xlsx"
	fmt.Println("打开文件...")
	f, err := excelize.OpenFile(path)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()
	fmt.Println("读取行...")
	rows, err := f.GetRows(f.GetSheetName(0))
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("遍历行...")
	var data []string
	for _, row := range rows {
		// fmt.Println(row)
		for _, cell := range row {
			data = append(data, cell+",")
		}
	}
	fmt.Println("写入文件...")
	utils.WriteLines("data.txt", data)
}
